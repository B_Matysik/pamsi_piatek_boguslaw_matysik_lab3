#include <iostream>
#include <cstdlib>
#include <cstring>
#include <stack>

using namespace std;

///////////////////////////////////////////////////////////////////////////
class StosSTL{
    stack <int> stosik;

public:
    void dodaj(int x);
    void usun();
    bool pusty();
    int rozmiar();
    int gora();
    void wyswietl_zawartosc();
    void wyczysc();

};

void StosSTL::dodaj(int x){
    stosik.push(x);
    cout << "Dodano: '" << stosik.top() << "' na stos" << endl;
}

void StosSTL::usun(){
    if(stosik.empty()==true){
        cout << "Sprytnie, ale stosik jest pusty, nie mozna usunac elementu:)" << endl;}

    else{
    cout << "Usunieto: '" << stosik.top() << "' ze stosu" << endl;
    stosik.pop(); }
    }

bool StosSTL::pusty(){
    if(stosik.empty()==true){
        cout << "Stos jest pusty"<< endl;
        return true; }
    else {
        cout << "Stos nie jest pusty"<< endl;
        return false; }
}

int StosSTL::rozmiar(){
    return stosik.size();
    }

int StosSTL::gora(){
    return stosik.top();
    }

void StosSTL::wyczysc(){
    if(stosik.empty()==true){
        cout << "Sprytnie, ale stosik jest pusty, nie mozna nic usunac :)" << endl;}

    else{
        while(stosik.empty()==false){
            stosik.pop();}
        cout << "Stos zostal oprozniony" << endl;
        }
}

void StosSTL::wyswietl_zawartosc(){
    if(stosik.empty()==true){
        cout << "Sprytnie, ale stosik jest pusty, nie mozna nic wyswietlic:)" << endl;}

    else{
        int *tab = new int[stosik.size()];
        int x=stosik.size();
        cout << "Zawartosc stosu: " << endl;

        for(int i=0; i<x; i++){
            tab[i]=stosik.top();
            cout << tab[i] << endl;
            stosik.pop(); }

        for(int i=x; i>0; i--){
            stosik.push(tab[i-1]); }
        }
    }
///////////////////////////////////////////////////////////////////////////


int main()
{
    StosSTL stos;
    char menu='m';
    int koniec=1;
    cout << "//// IMPLEMENTACJA STOSU STL ////" << endl;

    while(koniec){
    switch(menu){

    case 'k':{
        koniec=0;
        break; }

    case 'd':{
        int liczba=0;
        cout << "Podaj, jaka liczbe chcesz dodac na stos: ";
        cin >> liczba;
        stos.dodaj(liczba);
        menu='x';
        break; }

    case 'u':{
        stos.usun();
        menu='x';
        break; }

    case 'r':{
        cout << "Aktualnie na stosie jest: '" << stos.rozmiar() << "' elementow" << endl;
        menu='x';
        break; }

    case 'e':{
        stos.pusty();
        menu='x';
        break; }

    case 'w':{
        stos.wyswietl_zawartosc();
        menu='x';
        break; }

    case 'o':{
        stos.wyczysc();
        menu='x';
        break; }

    case 'm':{
        cout << "Menu: " << endl << "m - pokaz menu\t\tk - zakoncz program" << endl;
        cout << "d - dodaj liczbe\tu - usun liczbe" << endl;
        cout << "w - wyswietl zawartosc\to - oprocznij liste" << endl;
        cout << "e - sprawdz, czy pusty\tr - sprawdz rozmiar" << endl;
        menu='x';
        break; }

    case 'x':{
        cout << "Twoj wybor: ";
        cin >> menu;
        break;}

    default: {
        cout << "Prosze wybrac ISTNIEJACA opcje menu!" << endl;
        menu = 'x';
        break;}
    }}

return 0;
}
