#include <iostream>
#include <cstdlib>
#include <cstring>
#include <stack>

using namespace std;

///////////////////////////////////////////////////////////////////////////
class StosSTL{
    stack <int> stosik;

public:
    string nazwa;
    void dodaj(int x);
    void usun();
    bool pusty();
    int rozmiar();
    int gora();
    void wyswietl_zawartosc();
    void wyczysc();

};

void StosSTL::dodaj(int x){
    stosik.push(x);
    //cout << "Dodano: '" << stosik.top() << "' na stos: " <<nazwa<< endl;
}

void StosSTL::usun(){
    if(stosik.empty()==true){
        cout << "Sprytnie, ale stosik jest pusty, nie mozna usunac elementu:)" << endl;
        }

    else{
    //cout << "Usunieto: '" << stosik.top() << "' ze stosu: "<<nazwa << endl;
    stosik.pop(); }
    }

bool StosSTL::pusty(){
    if(stosik.empty()==true){
        cout << "Stos jest pusty"<< endl;
        return true; }
    else {
        cout << "Stos nie jest pusty"<< endl;
        return false; }
}

int StosSTL::rozmiar(){
    return stosik.size();
    }

int StosSTL::gora(){
    return stosik.top();
    }

void StosSTL::wyczysc(){
    if(stosik.empty()==true){
        cout << "Sprytnie, ale stosik jest pusty, nie mozna nic usunac :)" << endl;
        }

    else{
        while(stosik.empty()==false){
            stosik.pop();}
        cout << "Stos zostal oprozniony" << endl;
        }
}

void StosSTL::wyswietl_zawartosc(){
    if(stosik.empty()==true){
        cout << "Stos: " << nazwa<< " jest pusty" << endl;
        }

    else{
        int *tab = new int[stosik.size()];
        int x=stosik.size();
        cout << "Zawartosc stosu: "<< nazwa<< ": " << endl;

        for(int i=0; i<x; i++){
            tab[i]=stosik.top();
            cout << tab[i] << endl;
            stosik.pop(); }

        for(int i=x; i>0; i--){
            stosik.push(tab[i-1]); }
        }
    }
///////////////////////////////////////////////////////////////////////////
void Przekladanie(int krazki, StosSTL &Pierwszy, StosSTL &Drugi, StosSTL &Trzeci)
{
  if (krazki > 0) {
    Przekladanie(krazki-1, Pierwszy, Trzeci, Drugi);

    cout << "Przekladam ze stosu numer " << Pierwszy.nazwa << " na stos numer " << Trzeci.nazwa << endl;
    Trzeci.dodaj(Pierwszy.gora());
    Pierwszy.usun();

    Przekladanie(krazki-1, Drugi, Pierwszy, Trzeci); }
}
///////////////////////////////////////////////////////////////////////////


int main()
{
    StosSTL Pierwszy, Drugi, Trzeci;
    int krazki=0;

    Pierwszy.nazwa="Jeden";
    Drugi.nazwa="Dwa";
    Trzeci.nazwa="Trzy";

    cout << "Podaj liczbe krazkow na pierwszym stosie: ";
    cin >> krazki;

    for (int i=krazki; i!=0; i--){
    Pierwszy.dodaj(i); }

    Pierwszy.wyswietl_zawartosc();
    Drugi.wyswietl_zawartosc();
    Trzeci.wyswietl_zawartosc();
    cout << endl << endl;

    Przekladanie(krazki, Pierwszy, Drugi, Trzeci);

    cout << endl << endl;
    Pierwszy.wyswietl_zawartosc();
    Drugi.wyswietl_zawartosc();
    Trzeci.wyswietl_zawartosc();
return 0;
}
