#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;
int STALA=1000;

//////////////////////////////////////////////////////////////////////////
class StosMoj{
    int idx;
    int rozm;
    int *tab = new int[STALA];
    int *bufor;
    void rozszerzanie();


public:
    StosMoj();
    void dodaj(int x);
    void usun();
    bool pusty();
    int rozmiar();
    int gora();
    void wyswietl_zawartosc();
    void wyczysc();

};

StosMoj::StosMoj()
:idx(0), rozm(STALA)
{}

void StosMoj::rozszerzanie(){
    bufor = new int[rozm];
    for(int i=0; i<rozm; i++)
        bufor[i]=tab[i];
    delete[] tab;
    tab = new int [rozm+STALA];
    for(int i=0; i<rozm; i++)
        tab[i]=bufor[i];
    delete[] bufor;
    rozm=rozm+STALA;
}

void StosMoj::dodaj(int x){
    tab[idx]=x;
    idx++;
    cout << "Dodano: '" << tab[idx-1] << "' na stos" << endl;
    if (idx==rozm)
        rozszerzanie();
}

void StosMoj::usun(){
    if(idx==0){
        cout << "nie mozna nic wyswietlic, stos PUSTY" << endl;}

    else{
    cout << "Usunieto: '" << tab[idx-1] << "' ze stosu" << endl;
    tab[idx-1]=0;
    idx--;}
    }

bool StosMoj::pusty(){
    if(idx==0){
        cout << "Stos jest pusty"<< endl;
        return true; }
    else {
        cout << "Stos nie jest pusty"<< endl;
        return false; }
}

int StosMoj::rozmiar(){
    return idx;
    }

int StosMoj::gora(){
    return tab[idx-1];
    }

void StosMoj::wyczysc(){
    if(idx==0){
        cout << "nie mozna nic wyswietlic, stos PUSTY" << endl;}

    else{
        while(idx!=0){
            usun();}
        cout << "Stos zostal oprozniony" << endl;
        }
}

void StosMoj::wyswietl_zawartosc(){
    if(idx==0){
        cout << "nie mozna nic wyswietlic, stos PUSTY" << endl;}

    else{
        cout << "Zawartosc stosu: " << endl;
        for(int i=idx; i>0; i--){
            cout << tab[i-1] << endl; }
        }
    }
///////////////////////////////////////////////////////////////////////////


int main()
{
    StosMoj stos;
    char menu='m';
    int koniec=1;
    cout << "//// IMPLEMENTACJA STOSU powiekszanego co stala ////" << endl;

    while(koniec){
    switch(menu){

    case 'k':{
        koniec=0;
        break; }

    case 'd':{
        int liczba=0;
        cout << "Podaj, jaka liczbe chcesz dodac na stos: ";
        cin >> liczba;
        stos.dodaj(liczba);
        menu='x';
        break; }

    case 'u':{
        stos.usun();
        menu='x';
        break; }

    case 'r':{
        cout << "Aktualnie na stosie jest: '" << stos.rozmiar() << "' elementow" << endl;
        menu='x';
        break; }

    case 'e':{
        stos.pusty();
        menu='x';
        break; }

    case 'w':{
        stos.wyswietl_zawartosc();
        menu='x';
        break; }

    case 'o':{
        stos.wyczysc();
        menu='x';
        break; }

    case 'm':{
        cout << "Menu: " << endl << "m - pokaz menu\t\tk - zakoncz program" << endl;
        cout << "d - dodaj liczbe\tu - usun liczbe" << endl;
        cout << "w - wyswietl zawartosc\to - oprocznij liste" << endl;
        cout << "e - sprawdz, czy pusty\tr - sprawdz rozmiar" << endl;
        menu='x';
        break; }

    case 'x':{
        cout << "Twoj wybor: ";
        cin >> menu;
        break;}

    default: {
        cout << "Prosze wybrac ISTNIEJACA opcje menu!" << endl;
        menu = 'x';
        break;}
    }}

return 0;
}
