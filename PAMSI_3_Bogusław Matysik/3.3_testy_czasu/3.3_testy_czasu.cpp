
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <stack>
#include <sys/time.h>
#include "Timer.h"
using namespace std;
int ROZMIAR=1000;
int STALA=1000;



//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class StosMoj2{
    int idx;
    int rozm;
    int *tab = new int[STALA];
    int *bufor;
    void rozszerzanie();


public:
    StosMoj2();
    void dodaj(int x);
    void usun();
    bool pusty();
    int rozmiar();
    int gora();
    void wyswietl_zawartosc();
    void wyczysc();

};

StosMoj2::StosMoj2()
:idx(0), rozm(STALA)
{}

void StosMoj2::rozszerzanie(){
    bufor = new int[rozm];
    for(int i=0; i<rozm; i++)
        bufor[i]=tab[i];
    delete[] tab;
    tab = new int [rozm+STALA];
    for(int i=0; i<rozm; i++)
        tab[i]=bufor[i];
    delete[] bufor;
    rozm=rozm+STALA;
}

void StosMoj2::dodaj(int x){
    tab[idx]=x;
    idx++;
    //cout << "Dodano: '" << tab[idx-1] << "' na stos" << endl;
    if (idx==rozm)
        rozszerzanie();
}

void StosMoj2::usun(){
    if(idx==0){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna usunac elementu:)" << endl;
        }

    else{
    //cout << "Usunieto: '" << tab[idx-1] << "' ze stosu" << endl;
    tab[idx-1]=0;
    idx--;}
    }

bool StosMoj2::pusty(){
    if(idx==0){
        //cout << "Stos jest pusty"<< endl;
        return true; }
    else {
        //cout << "Stos nie jest pusty"<< endl;
        return false; }
}

int StosMoj2::rozmiar(){
    return idx;
    }

int StosMoj2::gora(){
    return tab[idx-1];
    }

void StosMoj2::wyczysc(){
    if(idx==0){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna nic usunac :)" << endl;
        }

    else{
        while(idx!=0){
            usun();}
        //cout << "Stos zostal oprozniony" << endl;
        }
}

void StosMoj2::wyswietl_zawartosc(){
    if(idx==0){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna nic wyswietlic:)" << endl;
        }

    else{
        cout << "Zawartosc stosu: " << endl;
        for(int i=idx; i>0; i--){
            cout << tab[i-1] << endl; }
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class StosSTL{
    stack <int> stosik;

public:
    void dodaj(int x);
    void usun();
    bool pusty();
    int rozmiar();
    int gora();
    void wyswietl_zawartosc();
    void wyczysc();

};

void StosSTL::dodaj(int x){
    stosik.push(x);
    //cout << "Dodano: '" << stosik.top() << "' na stos" << endl;
}

void StosSTL::usun(){
    if(stosik.empty()==true){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna usunac elementu:)" << endl;
        }

    else{
    //cout << "Usunieto: '" << stosik.top() << "' ze stosu" << endl;
    stosik.pop(); }
    }

bool StosSTL::pusty(){
    if(stosik.empty()==true){
        //cout << "Stos jest pusty"<< endl;
        return true; }
    else {
        //cout << "Stos nie jest pusty"<< endl;
        return false; }
}

int StosSTL::rozmiar(){
    return stosik.size();
    }

int StosSTL::gora(){
    return stosik.top();
    }

void StosSTL::wyczysc(){
    if(stosik.empty()==true){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna nic usunac :)" << endl;
        }

    else{
        while(stosik.empty()==false){
            stosik.pop();}
        //cout << "Stos zostal oprozniony" << endl;
        }
}

void StosSTL::wyswietl_zawartosc(){
    if(stosik.empty()==true){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna nic wyswietlic:)" << endl;
        }

    else{
        int *tab = new int[stosik.size()];
        int x=stosik.size();
        cout << "Zawartosc stosu: " << endl;

        for(int i=0; i<x; i++){
            tab[i]=stosik.top();
            cout << tab[i] << endl;
            stosik.pop(); }

        for(int i=x; i>0; i--){
            stosik.push(tab[i-1]); }
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class StosMoj{
    int idx;
    int rozm;
    int *tab = new int[STALA];
    int *bufor;
    void rozszerzanie();


public:
    StosMoj();
    void dodaj(int x);
    void usun();
    bool pusty();
    int rozmiar();
    int gora();
    void wyswietl_zawartosc();
    void wyczysc();

};

StosMoj::StosMoj()
:idx(0), rozm(STALA)
{}

void StosMoj::rozszerzanie(){
    bufor = new int[rozm];
    for(int i=0; i<rozm; i++)
        bufor[i]=tab[i];
    delete[] tab;
    tab = new int [rozm+STALA];
    for(int i=0; i<rozm; i++)
        tab[i]=bufor[i];
    delete[] bufor;
    rozm=rozm+STALA;
}

void StosMoj::dodaj(int x){
    tab[idx]=x;
    idx++;
    //cout << "Dodano: '" << tab[idx-1] << "' na stos" << endl;
    if (idx==rozm)
        rozszerzanie();
}

void StosMoj::usun(){
    if(idx==0){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna usunac elementu:)" << endl;
        }

    else{
    //cout << "Usunieto: '" << tab[idx-1] << "' ze stosu" << endl;
    tab[idx-1]=0;
    idx--;}
    }

bool StosMoj::pusty(){
    if(idx==0){
        //cout << "Stos jest pusty"<< endl;
        return true; }
    else {
        //cout << "Stos nie jest pusty"<< endl;
        return false; }
}

int StosMoj::rozmiar(){
    return idx;
    }

int StosMoj::gora(){
    return tab[idx-1];
    }

void StosMoj::wyczysc(){
    if(idx==0){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna nic usunac :)" << endl;
        }

    else{
        while(idx!=0){
            usun();}
        //cout << "Stos zostal oprozniony" << endl;
        }
}

void StosMoj::wyswietl_zawartosc(){
    if(idx==0){
        //cout << "Sprytnie, ale stosik jest pusty, nie mozna nic wyswietlic:)" << endl;
        }

    else{
        cout << "Zawartosc stosu: " << endl;
        for(int i=idx; i>0; i--){
            cout << tab[i-1] << endl; }
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Element{
    int liczba;
    Element *nastepny;
    friend class Lista;

    public:
    Element();
};

Element::Element()
:nastepny(0)
{}
///////////////////////////////////////////////////////////////////////////
class Lista{
    Element *pierwszy;

public:
    Lista();
    void dodaj_element(int x);
    void usun_element(int x);
    void wyczysc();
    void wyswietl_zawartosc();
    int rozmiar();

};

Lista::Lista()
:pierwszy(0)
{}

void Lista::dodaj_element(int x){
    Element *nowy = new Element;
    nowy->liczba=x;

    if(pierwszy==0)
        pierwszy = nowy;

    else {
        Element *pomoc = pierwszy;

        while(pomoc->nastepny)
            pomoc = pomoc->nastepny;

        pomoc->nastepny = nowy;
        nowy->nastepny = 0;

    }
    //cout << "Dodano element: '" << nowy->liczba << "' na koniec listy" << endl;
}

void Lista::usun_element(int x){
    int zm=rozmiar();
    Element *pomoc = pierwszy;

    if(x==1){                       // je¿eli element jest pierwszy
        pierwszy = pomoc->nastepny;
        delete pomoc; }

    if(x>1 && x<=zm){
        for(int i=1; i<x-1; i++)
            pomoc=pomoc->nastepny;      //znajduje poprzedni element (od usuwanego)

        Element *usuwany=pomoc->nastepny;
        if(x!=zm)                   // je¿eli element nie jest ostatni
            pomoc->nastepny=usuwany->nastepny;
        else                        // jezeli usuwamy ostatni element
            pomoc->nastepny=0;
        delete usuwany; }

     if(x<1 || x>zm){
        cout << "Prosze wybrac istniejacy element, czyli od 1 do " << zm << " !!!" << endl << "Podaj element do usuniecia: ";
        cin >> x;
        usun_element(x); }
    }

void Lista::wyczysc(){
    int zm=rozmiar();
    if(!zm){
        //cout << "Sprytnie, ale lista jest pusta, nie mozna z niej nic usunac :)" << endl;
        }

    else{

        while(pierwszy){
            Element *usuwany=pierwszy;
            pierwszy=pierwszy->nastepny;
            delete usuwany; }

        //cout << "Lista zostala oprozniona" << endl;
        }
}

int Lista::rozmiar(){
    Element *pomoc = pierwszy;
    int rozmiar=0;

        while(pomoc){
            pomoc = pomoc->nastepny;
            rozmiar++; }

    return rozmiar;
    }

void Lista::wyswietl_zawartosc(){
    Element *pomoc = pierwszy;

    if(pierwszy==0)
        cout << "Lista jest pusta, nie mam Ci nic do pokazania" << endl;
    else{

        cout << "Zawartosc listy: " << endl;
        while(pomoc){
            cout << pomoc->liczba << endl;
            pomoc = pomoc->nastepny; }
        }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


int main(){
int ilosc;
cout << "PROGRAM IMPLEMENTUJACY STOS NA PODSTAWIE \n-STL \n-TABLICY Z ROZSZERZENIEM O STALA,"<< endl;
cout << "    -Z ROZSZERZANIEM DWUKROTNYM  \n-LISTY." << endl<<endl;
cout << "Podaj ilosc losowych danych, ktore maja byc wrzucane na stosy: ";
cin >> ilosc;

    StosSTL * STL = new StosSTL;
    StosMoj * MojC = new StosMoj;
    StosMoj2 *Moj2 = new StosMoj2;
    Lista * StosLista = new Lista;



//#############################################################################

// WINDOWS + linx unix mac

/////////////////////////////////////// STL ///
    Timer timer1;
    // start timer
    timer1.start();

    for(int i=0; i<ilosc; i++){
    STL->dodaj(rand()); }

    // stop timer
    timer1.stop();
    // compute and print the elapsed time in millisec
    cout<< "STL: " << timer1.getElapsedTimeInMilliSec() << " ms.\n";

    delete STL;




    /////////////////////////////////////// MojC ///
    Timer timer2;

    // start timer
    timer2.start();

    for(int i=0; i<ilosc; i++){
    MojC->dodaj(rand()); }

    // stop timer
    timer2.stop();
    // compute and print the elapsed time in millisec
    cout << "Moj+C: "<< timer2.getElapsedTimeInMilliSec() << " ms.\n";
    delete MojC;



    /////////////////////////////////////// Moj2 ///
    Timer timer3;

    // start timer
    timer3.start();

    for(int i=0; i<ilosc; i++){
    Moj2->dodaj(rand()); }

    // stop timer
    timer3.stop();;
    // compute and print the elapsed time in millisec
     cout << "Moj*2: "<< timer3.getElapsedTimeInMilliSec() << " ms.\n";
    delete Moj2;



    /////////////////////////////////////// Lista ///
    Timer timer4;

    // start timer
    timer4.start();

    for(int i=0; i<ilosc; i++){
    StosLista->dodaj_element(rand()); }

    // stop timer
    timer4.stop();;
    // compute and print the elapsed time in millisec
   cout << "Lista: "<< timer4.getElapsedTimeInMilliSec() << " ms.\n";
    delete StosLista;

return 0;
}
