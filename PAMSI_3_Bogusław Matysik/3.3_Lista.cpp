#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;


///////////////////////////////////////////////////////////////////////////
class Element{
    int liczba;
    Element *nastepny;
    friend class Lista;

    public:
    Element();
};

Element::Element()
:nastepny(0)
{}
///////////////////////////////////////////////////////////////////////////
class Lista{
    Element *pierwszy;

public:
    Lista();
    void dodaj_element(int x);
    void usun_element(int x);
    void wyczysc();
    void wyswietl_zawartosc();
    int rozmiar();

};

Lista::Lista()
:pierwszy(0)
{}

void Lista::dodaj_element(int x){
    Element *nowy = new Element;
    nowy->liczba=x;

    if(pierwszy==0)
        pierwszy = nowy;

    else {
        Element *pomoc = pierwszy;

        while(pomoc->nastepny)
            pomoc = pomoc->nastepny;

        pomoc->nastepny = nowy;
        nowy->nastepny = 0;

    }
    //cout << "Dodano element: '" << nowy->liczba << "' na koniec listy" << endl;
}

void Lista::usun_element(int x){
    int zm=rozmiar();
    Element *pomoc = pierwszy;

    if(x==1){                       // je�eli element jest pierwszy
        pierwszy = pomoc->nastepny;
        delete pomoc; }

    if(x>1 && x<=zm){
        for(int i=1; i<x-1; i++)
            pomoc=pomoc->nastepny;      //znajduje poprzedni element (od usuwanego)

        Element *usuwany=pomoc->nastepny;
        if(x!=zm)                   // je�eli element nie jest ostatni
            pomoc->nastepny=usuwany->nastepny;
        else                        // jezeli usuwamy ostatni element
            pomoc->nastepny=0;
        delete usuwany; }

     if(x<1 || x>zm){
        cout << "Prosze wybrac istniejacy element, czyli od 1 do " << zm << " !!!" << endl << "Podaj element do usuniecia: ";
        cin >> x;
        usun_element(x); }
    }

void Lista::wyczysc(){
    int zm=rozmiar();
    if(!zm){
        //cout << "Sprytnie, ale lista jest pusta, nie mozna z niej nic usunac :)" << endl;
        }

    else{

        while(pierwszy){
            Element *usuwany=pierwszy;
            pierwszy=pierwszy->nastepny;
            delete usuwany; }

        //cout << "Lista zostala oprozniona" << endl;
        }
}

int Lista::rozmiar(){
    Element *pomoc = pierwszy;
    int rozmiar=0;

        while(pomoc){
            pomoc = pomoc->nastepny;
            rozmiar++; }

    return rozmiar;
    }

void Lista::wyswietl_zawartosc(){
    Element *pomoc = pierwszy;

    if(pierwszy==0)
        cout << "Lista jest pusta, nie mam Ci nic do pokazania" << endl;
    else{

        cout << "Zawartosc listy: " << endl;
        while(pomoc){
            cout << pomoc->liczba << endl;
            pomoc = pomoc->nastepny; }
        }
    }

///////////////////////////////////////////////////////////////////////////


int main()
{
    int ilosc=0;
    Lista *nowa = new Lista;
    char menu='p';
    int koniec=1;
    cout << "//// OBIEKTOWA IMPLEMENTACJA LISTY JEDNOKIERUNKOWEJ ////" << endl<<endl;

    while(koniec){
    switch(menu){
    case 'p':{
        cout << "Podaj, ile liczb dodac do listy: ";
        cin >> ilosc;
        for(int i=1; i<ilosc+1; i++)
            nowa->dodaj_element(rand()%50);
        cout << "Aktualny rozmiar: " << nowa->rozmiar() << endl << endl;
        menu='m';
        break;}

    case 'k':{
        koniec=0;
        break; }

    case 'd':{
        int liczba=0;
        cout << "Podaj, jaka liczbe chcesz dodac na liste: ";
        cin >> liczba;
        nowa->dodaj_element(liczba);
        menu='w';
        break; }

    case 'u':{
        int liczba=0;
        cout << "Ktory element usunac?: ";
        cin >> liczba;
        nowa->usun_element(liczba);
        menu='w';
        break; }

    case 'w':{
        nowa->wyswietl_zawartosc();
        menu='x';
        break; }

    case 'o':{
        nowa->wyczysc();
        menu='x';
        break; }

    case 'm':{
        cout << "Menu: " << endl << "m - pokaz menu\t\tk - zakoncz program" << endl;
        cout << "d - dodaj liczbe\tu - usun liczbe" << endl;
        cout << "w - wyswietl zawartosc\to - oprocznij liste" << endl;
        menu='x';
        break; }

    case 'x':{
        cout << "Twoj wybor: ";
        cin >> menu;
        break;}

    default: {
        cout << "Prosze wybrac ISTNIEJACA opcje menu!" << endl;
        menu = 'x';
        break;}
    }}
    return 0;
}
